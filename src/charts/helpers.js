export const colors = ['#1f78d1', '#1aaa55', '#fc9403', '#6666c4', '#db3b21'];

export function alphaColors(alpha) {
  const convertColorValue = val => parseInt(val, 16);
  return colors.map(color =>
    `rgba(${convertColorValue(color.substring(1, 3))},${convertColorValue(color.substring(3, 5))},${convertColorValue(color.substring(5))},${alpha})`);
}
