import Vue from 'vue';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import App from './App.vue';
import router from './router';
import store from './store/index';

Vue.config.productionTip = false;

document.addEventListener(
  'DOMContentLoaded',
  () =>
    new Vue({
      el: '#app',
      router,
      store,
      render: h => h(App),
    }),
);
