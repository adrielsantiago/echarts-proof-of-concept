import * as types from './mutation_types';

export default {
  [types.SET_COMMIT_DATA](state, data) {
    state.commitData = data;
  },

  [types.TOGGLE_DATA_IS_LOADING](state) {
    state.dataIsLoading = !state.dataIsLoading;
  },
};
