import randomNumber from '@/helpers';
import * as types from './mutation_types';

export const fetchCommitData = ({ dispatch }) => {
  dispatch('requestCommitData');

  const min = 5;
  const commitData = [[], [], [], []];
  for (let j = 0; j < 12; j++) {
    const max = 85;
    const lastIndex = commitData.length - 1;
    const data = [];
    let sum = 0;
    for (let i = 0; i < lastIndex; i++) {
      const newMax = max - sum - min * (lastIndex - i);
      const random = randomNumber(min, newMax);
      commitData[i].push(random);
      sum += random;
    }
    commitData[lastIndex].push(100 - sum);
  }

  dispatch('fetchCommitDataSuccess', commitData);
  dispatch('requestCommitData');
};

export const requestCommitData = ({ commit }) => commit(types.TOGGLE_DATA_IS_LOADING);

export const fetchCommitDataSuccess = ({ commit }, data) => {
  commit(types.SET_COMMIT_DATA, data);
};
